import requests
from bs4 import BeautifulSoup
import pandas as pd

def scrape_data():
    # URL of the page to scrape
    url = 'https://fbref.com/en/comps/23/schedule/Talleres-Schedule'

    # Send a GET request to the URL
    response = requests.get(url)
    # Parse the HTML content of the page with BeautifulSoup
    soup = BeautifulSoup(response.text, 'html.parser')

    # Find the table containing match results and player stats
    table = soup.find('table', {'class': 'stats_table'})

    # Initialize lists to store the data
    dates = []
    opponents = []
    results = []
    goals_scored = []
    assists = []
    yellow_cards = []
    red_cards = []

    # Iterate through each row in the table
    for row in table.find_all('tr'):
        cells = row.find_all('td')
        if len(cells) > 1:  # to ensure it's not a header row
            dates.append(cells[0].text.strip())
            opponents.append(cells[1].text.strip())
            results.append(cells[2].text.strip())
            goals_scored.append(cells[3].text.strip())
            assists.append(cells[4].text.strip())
            yellow_cards.append(cells[5].text.strip())
            red_cards.append(cells[6].text.strip())

    # Create a DataFrame from the scraped data
    data = {
        'Date': dates,
        'Opponent': opponents,
        'Result': results,
        'Goals Scored': goals_scored,
        'Assists': assists,
        'Yellow Cards': yellow_cards,
        'Red Cards': red_cards
    }
    df = pd.DataFrame(data)

    # Print the DataFrame
    print(df)

    # Optionally, update this data to a database or a file
    # df.to_csv('talleres_match_data.csv', index=False)

# Run the function
scrape_data()